# Docker 関連

## 未使用データの削除

```bash
docker volume prune
docker image prune -a
docker system prune --volumes
```

# 初回起動

gitから取得した際に実施する

※初回は「master」のブランチからpullして,下記のコマンドを実行後ほかのブランチをとってくるように.

```bash
docker-compose build
docker-compose run --rm web sh -c "npx create-react-app /usr/src/app --template typescript"
docker-compose up
```

# Docker 系
## Docker file

FROM [image名]　＞
　実行するDocker Imageを指定

WORKDIR /　＞　コンテナ内の作業ディレクトリ

## Docker-compose
```bash
version: '3' #docker-composeのversion.固定
services: # 構築するサービス情報
  web:
    build: # Dockerfileのディレクトリを指定
      context: .
      dockerfile: Dockerfile-react
    container_name: tsx-react # コンテナ名
    volumes: # 接続するローカルの作業ディレクトリを指定
      - ./my-app:/usr/src/app
    command: sh -c "yarn start" # コンテナ内で実行されるコマンド
    tty: true # コンテナが勝手に終了しないように
    ports: # 外部から接続するためのport指定. ホスト側:コンテナ側
      - "30000:3000"
    environment:
      - WATCHPACK_POLLING=true # ソース変更時にホットリロード(更新が反映)されるように
      # - CHOKIDDAR_USEPOLLING=true 古いreactでのホットリロードの場合.
```

# React
## 使用するライブラリ
```bash
npm install react-router-dom
npm install @types/react-router-dom
npm install date-fns-timezone
npm install moment
```

## 未使用 React インポートの削除
npx react-codemod update-react-imports